package com.kto.sports.online.game.ui.app.check_loading

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class CheckLoadingViewModel: ViewModel() {
    private val _progressOfLoading = MutableStateFlow(1f)
    val progressOfLoading = _progressOfLoading.asStateFlow()

    var onFinishLoading: (()-> Unit)? = null

    init {
        viewModelScope.launch {
            repeat(99) {
                delay(
                    15 + 15 - 15
                )
                _progressOfLoading.update {
                    it + 1
                }
            }
            if (onFinishLoading != null) {
                onFinishLoading?.invoke()
            }
        }
    }
}