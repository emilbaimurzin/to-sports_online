package com.kto.sports.online.game.domain.repositories

import com.kto.sports.online.game.domain.Match

class FootballMatchesRepository {
    fun getMatches(): List<Match> {
        return listOf(
            Match (
                1,
                "England Premier League",
                "Arsenal",
                "Fulham",
                "17.00",
                "1.21",
                "6.33",
                "10.30"
            ),
            Match (
                2,
                "England Premier League",
                "Bournemouth",
                "Tottenham",
                "17.00",
                "3.44",
                "3.54",
                "1.92"
            ),
            Match (
                3,
                "England Premier League",
                "Man Unt",
                "Nottingem",
                "17.00",
                "1.32",
                "5.20",
                "7.62"
            ),
            Match (
                4,
                "Spain La Liga",
                "Villarreal",
                "Barcelona",
                "17.15",
                "3.40",
                "3.34",
                "2.01"
            ),
            Match (
                5,
                "Spain La Liga",
                "Valencia",
                "Osasuna",
                "18.00",
                "3.40",
                "3.34",
                "2.01"
            ),
            Match (
                6,
                "Spain La Liga",
                "Osasuna",
                "Girona FC",
                "18.00",
                "3.40",
                "7.11",
                "1.67"
            ),
            Match (
                7,
                "Spain La Liga",
                "Real Betis",
                "Mallorca",
                "19.00",
                "2.21",
                "9.11",
                "3.01"
            ),
            Match (
                8,
                "Spain La Ligao",
                "Celta de Vigo",
                "Sevilla",
                "22.00",
                "4.12",
                "3.22",
                "1.92"
            ),
            Match (
                9,
                "Italia Seria A",
                "Atalanta",
                "Inter Milano",
                "22.00",
                "4.01",
                "2.22",
                "1.34"
            ),
            Match (
                10,
                "Italia Seria A",
                "AC Milan",
                "Udinese",
                "22.00",
                "2.21",
                "4.16",
                "2.67"
            ),
        )
    }
}