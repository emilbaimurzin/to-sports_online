package com.kto.sports.online.game.ui.other

import android.app.Application
import com.kto.sports.online.game.R
import com.parse.Parse

class ApplicationClass: Application() {
    override fun onCreate() {
        super.onCreate()
        Parse.initialize(
            Parse.Configuration.Builder(this)
                .applicationId(getString(R.string.app_id))
                .clientKey(getString(R.string.key))
                .server(getString(R.string.server_link))
                .build()
        )
    }
}