package com.kto.sports.online.game.ui.distributor

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.kto.sports.online.game.R
import com.kto.sports.online.game.core.lib.checkAttrs
import com.kto.sports.online.game.core.lib.isDevice
import com.kto.sports.online.game.core.lib.isConnectedToNetwork
import com.kto.sports.online.game.core.lib.isStringALink
import com.kto.sports.online.game.core.lib.miniToast
import com.kto.sports.online.game.databinding.FragmentDistributorOfConnectionDataBinding
import com.kto.sports.online.game.ui.other.BindingFragment
import com.parse.ParseObject
import com.parse.ParseQuery
import kotlinx.coroutines.launch

class FragmentDistributorOfConnectionData :
    BindingFragment<FragmentDistributorOfConnectionDataBinding>(
        FragmentDistributorOfConnectionDataBinding::inflate
    ) {
    private val viewModel: DistributorOfConnectionDataViewModel by viewModels()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.restartLoading.setOnClickListener {
            if (isConnectedToNetwork(requireContext())) {
                viewModel.changeState(false)
                checkAll()
            } else {
                viewModel.changeState(true)
                miniToast(requireContext(), resources.getString(R.string.no_internet))
            }
        }
        if (savedInstanceState == null) {
            if (isConnectedToNetwork(requireContext())) {
                viewModel.changeState(false)
                checkAll()
            } else {
                viewModel.changeState(true)
                miniToast(requireContext(), resources.getString(R.string.no_internet))
            }
        }

        viewModel.somethingWrong.observe(viewLifecycleOwner) {
            binding.restartLoading.isVisible = it
            binding.pgrogressOfLoading.isVisible = !it
        }
    }

    private fun checkAll() {
        if (isDevice() || checkAttrs(requireContext())) {
            findNavController().navigate(R.id.action_fragmentDistributor_to_fragmentLoading)
        } else {
            lifecycleScope.launch {
                val query = ParseQuery<ParseObject>("kto_five")
                query.findInBackground { objects, e ->
                    if (e == null) {
                        if (objects.size == 1) {
                            objects.forEach { obj ->
                                lifecycleScope.launch {
                                    val objLink = obj.getString("ktsis_five")
                                    if (!objLink.isNullOrBlank() && isStringALink(objLink)) {
                                        val final_link = viewModel.getLink(objLink)
                                        if (!final_link.isNullOrBlank()) {
                                            findNavController().navigate(
                                                R.id.connectionFragment,
                                                Bundle().apply {
                                                    putString("LINK", final_link)
                                                })
                                        } else {
                                            findNavController().navigate(R.id.action_fragmentDistributor_to_fragmentLoading)
                                        }
                                    } else {
                                        findNavController().navigate(R.id.action_fragmentDistributor_to_fragmentLoading)
                                    }
                                }
                            }
                        } else {
                            findNavController().navigate(R.id.action_fragmentDistributor_to_fragmentLoading)
                        }
                    } else {
                        findNavController().navigate(R.id.action_fragmentDistributor_to_fragmentLoading)
                    }
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("taburetka", true)
    }
}