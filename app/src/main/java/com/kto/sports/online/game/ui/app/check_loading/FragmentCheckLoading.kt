package com.kto.sports.online.game.ui.app.check_loading

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.kto.sports.online.game.R
import com.kto.sports.online.game.databinding.FragmentCheckLoadingBinding
import com.kto.sports.online.game.ui.other.BindingFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FragmentCheckLoading: BindingFragment<FragmentCheckLoadingBinding>(FragmentCheckLoadingBinding::inflate) {
    private val viewModel: CheckLoadingViewModel by viewModels()
    private val onFinishLoading by lazy {
        {
            lifecycleScope.launch(Dispatchers.Main) {
                findNavController().navigate(R.id.action_fragmentLoading_to_fragmentWelcome)
            }
            Unit
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.progressOfLoading.collect {
                    val width = binding.bgOfProgress.width / 100 * it
                    val lp = binding.progressOfLoading.layoutParams
                    lp.width = width.toInt()
                    binding.progressOfLoading.layoutParams = lp
                }
            }
        }
        viewModel.onFinishLoading = onFinishLoading
        requireActivity().onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

            }
        })
    }
}