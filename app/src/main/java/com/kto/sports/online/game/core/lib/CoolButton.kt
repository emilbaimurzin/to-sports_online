package com.kto.sports.online.game.core.lib

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.appcompat.widget.AppCompatImageView

class CoolImageButton(context: Context, attr: AttributeSet) : AppCompatImageView(context, attr) {
    init {
        this.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    v.animate()
                        .setDuration(100)
                        .scaleX(0.9f)
                        .scaleY(0.9f)
                        .start()
                    v.invalidate()
                }
                MotionEvent.ACTION_UP -> {
                    v.animate()
                        .setDuration(100)
                        .scaleX(1f)
                        .scaleY(1f)
                        .start()
                    v.invalidate()
                }
            }
            false
        }
    }
}