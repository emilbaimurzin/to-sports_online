package com.kto.sports.online.game.ui.app.welocome_user

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class WelcomeUserViewModel: ViewModel() {
    private val _welcomeState = MutableLiveData(1)
    val welcomeState: LiveData<Int> = _welcomeState

    var canNavigate = true

    fun changeWelcomeState() {
        _welcomeState.postValue(_welcomeState.value!! + 1)
        if (_welcomeState.value!! == 3) {
            canNavigate = false
        }
    }
}