package com.kto.sports.online.game.ui.app.football_matches

import androidx.lifecycle.ViewModel
import com.kto.sports.online.game.domain.repositories.FootballMatchesRepository

class FootBallMatchesViewModel: ViewModel() {
    private val repository = FootballMatchesRepository()
    val list = repository.getMatches()
}