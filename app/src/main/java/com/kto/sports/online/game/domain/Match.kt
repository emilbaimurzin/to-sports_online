package com.kto.sports.online.game.domain

import java.io.Serializable

data class Match (
    val id: Int,
    val title: String,
    val firstTeam: String,
    val secondTeam: String,
    val time: String,
    val one: String,
    val x: String,
    val two: String
): Serializable