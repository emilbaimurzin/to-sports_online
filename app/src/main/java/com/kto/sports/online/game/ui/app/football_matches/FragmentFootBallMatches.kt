package com.kto.sports.online.game.ui.app.football_matches

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isEmpty
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.kto.sports.online.game.R
import com.kto.sports.online.game.databinding.ContainerMatchBinding
import com.kto.sports.online.game.databinding.FragmentMatchesBinding
import com.kto.sports.online.game.ui.other.BindingFragment

class FragmentFootBallMatches :
    BindingFragment<FragmentMatchesBinding>(FragmentMatchesBinding::inflate) {
    private val viewModel: FootBallMatchesViewModel by viewModels()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

            }
        })

        if (binding.matchesLayout.isEmpty()) {
            viewModel.list.forEach {match ->
                val layout = ContainerMatchBinding.inflate(
                    LayoutInflater.from(requireContext()),
                    binding.matchesLayout,
                    false
                )
                layout.footballMatchTitle.text = match.title
                layout.footballMatchTime.text = match.time
                layout.nameOfTeam1.text = match.firstTeam
                layout.nameOfTeam2.text = match.secondTeam
                layout.etoKotocheCyfry.text = match.one
                layout.aEtoKorocheCyfraVKonce.text = match.two
                layout.aEtoTipoCifryPoSeredine.text = match.x

                layout.root.setOnClickListener {
                    findNavController().navigate(R.id.action_fragmentMatches_to_fragmentMatchContent, Bundle().apply {
                        putSerializable("MATCH", match)
                    })
                }
                binding.matchesLayout.addView(layout.root)
            }
        }
    }
}