package com.kto.sports.online.game.domain.repositories

import com.kto.sports.online.game.domain.internet.SomeKindOfNetworkInterface
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jsoup.Jsoup
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class DistributorOfConnectionDataRepository {
    suspend fun getStringToInternet(stringWeNeed: String): String? {
        return suspendCoroutine { continuation ->
            CoroutineScope(Dispatchers.IO).launch {
                val pieces = stringWeNeed.split("/")
                val mainPiece = pieces.dropLast(1).joinToString("/")
                val lastPiece = pieces.last()
                val retrofitImpl = Retrofit.Builder()
                    .baseUrl(mainPiece)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build()

                val internetInterface = retrofitImpl.create(SomeKindOfNetworkInterface::class.java)
                val call = internetInterface.getInternetHtmlPage(lastPiece)
                call.enqueue(object : Callback<String> {
                    override fun onResponse(call: Call<String>, response: Response<String>) {
                        if (response.isSuccessful) {
                            val htmlBody = response.body() ?: ""
                            val docs = Jsoup.parse(htmlBody)
                            val docsBody = docs.body()

                            continuation.resume(if (docsBody.text() == "") null else docsBody.text())
                        }
                    }

                    override fun onFailure(call: Call<String>, t: Throwable) {
                        continuation.resume(null)
                    }
                })
            }
        }
    }
}