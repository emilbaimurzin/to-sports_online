package com.kto.sports.online.game.ui.connection

import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Message
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.webkit.CookieManager
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import android.widget.LinearLayout.LayoutParams
import android.widget.ProgressBar
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import com.kto.sports.online.game.databinding.FragmentConnectionBinding
import com.kto.sports.online.game.ui.other.BindingFragment

class ConnectionFragment: BindingFragment<FragmentConnectionBinding>(FragmentConnectionBinding::inflate) {
    var filePath: ValueCallback<Array<Uri>>? = null
    private lateinit var connectionScreen: WebView
    private lateinit var progress: ProgressBar
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
        
        binding.root.removeAllViews()
        connectionScreen = WebView(requireContext())
        connectionScreen.layoutParams = ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)

        progress = ProgressBar(requireContext())
        progress.layoutParams = FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply {
            gravity = Gravity.CENTER
        }
        binding.root.addView(connectionScreen)
        binding.root.addView(progress)

        CookieManager.getInstance().setAcceptThirdPartyCookies(connectionScreen, true)
        connectionScreen.settings.javaScriptEnabled = true
        connectionScreen.settings.domStorageEnabled = true
        connectionScreen.settings.setSupportZoom(true)
        connectionScreen.settings.builtInZoomControls = true
        connectionScreen.settings.useWideViewPort = true
        connectionScreen.webViewClient = MyWebViewClient()
        connectionScreen.webChromeClient = MyWebChromeClient()
        if (savedInstanceState != null) {
            connectionScreen.restoreState(savedInstanceState)
        }
        connectionScreen.loadUrl(arguments?.getString("LINK") ?: "")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        connectionScreen.saveState(outState)
    }

    inner class MyWebViewClient : WebViewClient() {
        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            progress.visibility = View.VISIBLE
        }

        override fun shouldOverrideUrlLoading(
            view: WebView?,
            request: WebResourceRequest?
        ): Boolean {
            view?.loadUrl(request?.url.toString())
            return true
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            progress.visibility = View.GONE
        }

        override fun onReceivedError(
            view: WebView?,
            request: WebResourceRequest?,
            error: WebResourceError?
        ) {
            val errorCode = error?.errorCode
            val activity = view?.context as? Activity
            val context = view?.context

            if (errorCode != null && activity != null) {
                val errorMessage = when (errorCode) {
                    ERROR_CONNECT -> "Connection Error"
                    ERROR_TIMEOUT -> "Timeout Error"
                    else -> "Unspecified error"
                }

                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
            }
        }

        override fun onReceivedHttpError(
            view: WebView?,
            request: WebResourceRequest?,
            errorResponse: WebResourceResponse?
        ) {
            Log.e("error", errorResponse?.statusCode.toString())
        }
    }

    inner class MyWebChromeClient : WebChromeClient() {
        override fun onShowFileChooser(
            webView: WebView?,
            filePathCallback: ValueCallback<Array<Uri>>?,
            fileChooserParams: FileChooserParams?
        ): Boolean {
            filePath = filePathCallback

            val contentIntent = Intent(Intent.ACTION_GET_CONTENT)
            contentIntent.type = "*/*"
            contentIntent.addCategory(Intent.CATEGORY_OPENABLE)

            getFile.launch(contentIntent)
            return true
        }

        override fun onCreateWindow(
            view: WebView?,
            isDialog: Boolean,
            isUserGesture: Boolean,
            resultMsg: Message?
        ): Boolean {
            val newWebView = WebView(requireContext())
            val settings = newWebView.settings
            settings.javaScriptEnabled = true

            val transport = resultMsg?.obj as WebView.WebViewTransport
            transport.webView = newWebView
            resultMsg.sendToTarget()

            return true
        }
    }

    val getFile = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) {
        if (it.resultCode == Activity.RESULT_CANCELED) {
            filePath?.onReceiveValue(null)
        } else if (it.resultCode == Activity.RESULT_OK && filePath != null) {
            filePath!!.onReceiveValue(
                WebChromeClient.FileChooserParams.parseResult(it.resultCode, it.data)
            )
            filePath = null
        }
    }
}