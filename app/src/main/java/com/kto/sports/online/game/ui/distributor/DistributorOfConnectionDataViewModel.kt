package com.kto.sports.online.game.ui.distributor

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kto.sports.online.game.domain.repositories.DistributorOfConnectionDataRepository

class DistributorOfConnectionDataViewModel: ViewModel() {
    private val repository = DistributorOfConnectionDataRepository()

    private val _somethingWrong = MutableLiveData(false)
    val somethingWrong: LiveData<Boolean> = _somethingWrong

    fun changeState(value: Boolean) {
        _somethingWrong.postValue(value)
    }

    suspend fun getLink(link: String): String? {
        return repository.getStringToInternet(link)
    }
}