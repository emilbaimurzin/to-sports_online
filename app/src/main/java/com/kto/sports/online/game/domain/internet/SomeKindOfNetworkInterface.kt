package com.kto.sports.online.game.domain.internet

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface SomeKindOfNetworkInterface {
    @GET("{identifier}")
    fun getInternetHtmlPage(@Path("identifier") id: String): Call<String>
}