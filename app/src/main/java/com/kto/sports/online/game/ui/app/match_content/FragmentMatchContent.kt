package com.kto.sports.online.game.ui.app.match_content

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import com.kto.sports.online.game.R
import com.kto.sports.online.game.databinding.FragmentMatchContentBinding
import com.kto.sports.online.game.domain.Match
import com.kto.sports.online.game.ui.other.BindingFragment

class FragmentMatchContent :
    BindingFragment<FragmentMatchContentBinding>(FragmentMatchContentBinding::inflate) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().popBackStack()
            }
        })
        
        val match = arguments?.getSerializable("MATCH") as Match
        binding.footballMatchTitle.text = match.title
        binding.footballMatchTime.text = match.time
        binding.nameOfTeam1.text = match.firstTeam
        binding.nameOfTeam2.text = match.secondTeam
        binding.etoKotocheCyfry.text = match.one
        binding.aEtoKorocheCyfraVKonce.text = match.two

        binding.description.text = getString(
            when (match.id) {
                1 -> R.string.arsenal_fulham
                2 -> R.string.bournemouth_tottenham
                3 -> R.string.man_unt_nottingem
                4 -> R.string.villarreal_barcelona
                5 -> R.string.valencia_osasuna
                6 -> R.string.osasuna_giorna_fc
                7-> R.string.real_betis_mallorca
                8 -> R.string.celta_de_vigo_sevillia
                9 -> R.string.atlanta_inter_milano
                else -> R.string.ac_milan_udinese
            }
        )
    }
}