package com.kto.sports.online.game.ui.app.welocome_user

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.kto.sports.online.game.R
import com.kto.sports.online.game.databinding.FragmentWelcomeUserBinding
import com.kto.sports.online.game.ui.other.BindingFragment

class FragmentWelcomeUser: BindingFragment<FragmentWelcomeUserBinding>(FragmentWelcomeUserBinding::inflate) {
    private val viewModel: WelcomeUserViewModel by viewModels()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

            }
        })

        binding.buttonNextPage.setOnClickListener {
            viewModel.changeWelcomeState()
        }

        viewModel.welcomeState.observe(viewLifecycleOwner) {
            binding.welcomeText.text = if (it == 1) getString(R.string.welcome) else getString(R.string.welcome_2)
            if (it == 3 && viewModel.canNavigate) {
                findNavController().navigate(R.id.action_fragmentWelcome_to_fragmentMatches)
            }
        }
    }
}